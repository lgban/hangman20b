import React, {useState, useEffect} from 'react';
import {clearCanvas, Draw, draws} from './drawing';

const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
't', 'u', 'v', 'w', 'x', 'y', 'z'];

function App() {
  let [path, setPath] = useState();
  let [feedback, setFeedback] = useState("");

  let submitLetter = (letter) => {
    fetch("http://localhost:8010/proxy" + path, {
      method: "POST",
      headers: {'Content-Type': 'application/json'},
      body:  JSON.stringify({ "guess": letter })  
    }).then(response => console.log(response)).then( _ => updateScore());
  }

  let updateScore = () => {
    if (path)
      fetch("http://localhost:8010/proxy" + path)
      .then(response => response.json())
      .then(response => {
        setFeedback(response.feedback);
        var step = 8-response.remaining_turns;
        if (step >= 0)
          Draw(draws[step]);
        console.log(response)
      })
  }

  // eslint-disable-next-line
  useEffect(() => updateScore(), [path]);

  let reset = () => {
    fetch("http://localhost:8010/proxy/games", {
      method: "POST",
    }).then(response => {
      clearCanvas();
      setPath(response.headers.get('Location'));
    });
  }

  return (
    <div>
      <div style={{textAlign: "center", fontFamily: "monospace" , fontSize: 32, letterSpacing: "4px", fontWeight: "bold", color: "blue"}}>
        {feedback}
      </div>
      <div className="buttons" style={{maxWidth: 250}}>
        {
          alphabet.map((letter, index) =>
            <button
              onClick={() => submitLetter(letter)}
              key= {index}>
              {letter}
            </button>
          )
        }
      </div>
      <div className="buttons">
        <button id="reset" onClick={reset}>New game</button>
      </div>
    </div>
  );
}

export default App;
