defmodule Hangman do
  def score_guess({secret, correct, wrong, turns_left} = game, guess) when turns_left > 0 do
    cond do
      correct =~ guess or wrong =~ guess -> game
      secret =~ guess -> {secret, correct <> guess, wrong, turns_left}
      true -> {secret, correct, wrong <> guess, turns_left - 1}
    end
  end
  def score_guess(game, _guess), do: game

  def format_feedback({secret, correct, _wrong, _turns_left}) do
    String.split(secret, "", trim: true)
    |> Enum.map(fn e -> if correct =~ e, do: e, else: "-" end)
    |> Enum.join
  end
end
