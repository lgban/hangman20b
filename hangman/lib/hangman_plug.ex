defmodule Hangman.HangmanPlug do
  use Plug.Router

  plug :match
  plug Plug.Parsers, parsers: [:json], pass: ["application/json"], json_decoder: Poison
  plug :dispatch

  post "/games" do
    id = UUID.uuid1()
    Game.start_link(String.to_atom(id))
    conn
    |> put_resp_header("Access-Control-Expose-Headers", "Location")
    |> put_resp_header("Location", "/games/"<>id)
    |> send_resp(201, Poison.encode!(%{msg: "Welcome"}))
  end

  post "/games/:id" do
    %{"guess" => letter} = conn.body_params
    result = Game.submit_guess(String.to_atom(id), letter)
    conn
    |> send_resp(201, Poison.encode!(%{msg: "Letter submitted"}))
  end

  get "/games/:id" do
    result = Game.get_feedback(String.to_atom(id))
    conn
    |> send_resp(200, Poison.encode!(result))
  end

  match _ do
    send_resp(conn, 404, "Oops!")
  end
end
