defmodule GameTest do
  use ExUnit.Case
  import Mock

  test "Creates a process holding the game state" do
    with_mock Dictionary, [random_word: fn() -> "platypus" end] do
      {:ok, pid} = Game.start_link()
      assert is_pid(pid)
      assert pid != self()
      assert called Dictionary.random_word
    end
  end

  # @tag :skip
  test "Submit a guess for the secret word" do
    with_mock Dictionary, [random_word: fn() -> "platypus" end] do
      {:ok, pid} = Game.start_link()
      assert :ok = Game.submit_guess(pid,"t")
    end
  end

  # @tag :skip
  test "get feedback on our guesses - status playing" do
    with_mock Dictionary, [random_word: fn() -> "platypus" end] do
      {:ok, pid} = Game.start_link()
      Enum.each(~w[t p q], &(Game.submit_guess(pid, &1)))
      assert %{feedback: "p--t-p--", remaining_turns: 8, status: :playing} = Game.get_feedback(pid)
    end
  end

  @tag :skip
  test "get feedback on our guesses - lose" do
    with_mock Dictionary, [random_word: fn() -> "platypus" end] do
      {:ok, pid} = Game.start_link()
      Enum.each(~w[f g q q k v w m n r], &(Game.submit_guess(pid, &1)))
      assert %{feedback: "--------", remaining_turns: 0, status: :lose} = Game.get_feedback(pid)
    end
  end

  @tag :skip
  test "get feedback on our guesses - win" do
    with_mock Dictionary, [random_word: fn() -> "platypus" end] do
      {:ok, pid} = Game.start_link()
      Enum.each(~w[p x l a t y p u s], &(Game.submit_guess(pid, &1)))
      assert %{feedback: "platypus", remaining_turns: 8, status: :win} = Game.get_feedback(pid)
    end
  end
end
